
// Play audio on sumit button click (ruby-4.html specific)
if (window.location.href.includes('ruby-4')) {
    document.querySelector('.bell').addEventListener('click', (elem) => {
        const audio = document.createElement('audio');
        pauseAudio()
        audio.volume = 1;
        setTimeout(() => {
            playAudio(elem.target, audio);
        }, 1000);
    });
}

// Show inputs on focus (ruby-3.html and ruby-4.html specific)
document.querySelectorAll('.asio').forEach((element) => {
    element.addEventListener('focus', (elem) => {
        elem.target.classList.add('visible');
        if (elem.target.nextElementSibling.tagName == 'LABEL') {
            elem.target.nextElementSibling.classList.add('visible');
        }
    });
});
