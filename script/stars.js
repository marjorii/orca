// Code by Dudley Storey (https://codepen.io/dudleystorey/pen/QjaXKJ)

const canvas = document.querySelector('canvas'),
context = canvas.getContext('2d'),
stars = 600,
colorrange = [0,60,240];

for (let i = 0; i < stars; i++) {
	let x = Math.random() * canvas.offsetWidth;
    y = Math.random() * canvas.offsetHeight,
	radius = Math.random() * 2;
	context.beginPath();
	context.arc(x, y, radius, 0, 360);
	context.fillStyle = 'white';
	context.fill();
}

function getRandom(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
