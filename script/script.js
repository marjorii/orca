const menu = document.getElementById('menu');
const commands = document.getElementById('repeat-commands');
const mode = document.getElementById('change-mode');
const actionButtons = document.querySelectorAll('.action');

// Handle commands menu
document.addEventListener('keydown', function(e) {
    // ESC
    if (e.keyCode === 27) {
        // Toggle menu (input)
        menu.classList.remove('hide');
        menu.focus();
        setTimeout(() => {
            menu.classList.add('hide');
        }, 5000);
    }
});

menu.onkeydown = (e) =>  {

    // Repeat commands

    // c
    if (e.keyCode === 67) {
        readGuideline(commands);
        preventDef(commands);
    }

    // Add "debutant" (beginner) in page hash

    // m
    if (e.keyCode === 77) {
        if (!window.location.hash || window.location.hash == '#beginner') {
            window.location.hash = 'expert';
            mode.textContent = 'Mode confirmé';
        }
        else if (window.location.hash = '#expert') {
            window.location.hash = 'beginner';
            mode.textContent = 'Mode débutant';
        }
        else {
            return
        }

        readGuideline(mode);

        setTimeout(() => {
            switchMode();
        }, 100);

        preventDef(mode);
    }
}

// Keep hash for next page
if (window.location.hash) {
    switchMode();
}

document.querySelector('.continue').onclick = function(e) {
    if (window.location.hash) {
        this.href = this.href + window.location.hash;
    }
}



// UTILS

// Prevent default behaviour
function preventDef(elem) {
    elem.onclick = (e) => {
        e.preventDefault();
    }
}

// Read guideline
function readGuideline(elem) {
    elem.classList.remove('hide')
    elem.focus();
    setTimeout(() => {
        elem.blur();
        elem.classList.add('hide');

        menu.blur();
        menu.classList.add('hide');

    }, 100);
}

// Switch mode according to page hash by adjusting aria-labels
function switchMode() {
    // If beginner
    if (window.location.hash == '#beginner') {
        // Add additionnal comments
        document.querySelectorAll('.beginner').forEach((elem) => {
            elem.classList.remove('hide');
        });
        // Change aria-labels in action buttons
        actionButtons.forEach((actionButton) => {
            let label = actionButton.getAttribute('aria-label');
            if (label.includes('Appuyez')) {
                return
            }
            else {
                label = label[0].toLowerCase() + label.slice(1)
                // if (actionButton.classList.contains('continue')) {
                //     label = label.replace(label, 'Appuyez sur entrer pour ' + label);
                // }
                // else {
                //     label = label.replace(label, 'Appuyez sur entrer pour ' + label + ', tabulation pour continuer');
                // }
                label = label.replace(label, 'Appuyez sur entrer pour ' + label);
                actionButton.setAttribute('aria-label', label);
            }
        });
    }
    else if (window.location.hash == '#expert') {
        // Remove additionnal comments
        document.querySelectorAll('.beginner').forEach((elem) => {
            elem.classList.add('hide');
        });
        // Change aria-labels in action buttons
        actionButtons.forEach((actionButton) => {
            let label = actionButton.getAttribute('aria-label');
            if (label.includes('Appuyez')) {
                // label = label.replace('Appuyez sur entrer pour ', '').replace(', tabulation pour continuer', '');
                label = label.replace('Appuyez sur entrer pour ', '');
                label = label[0].toUpperCase() + label.slice(1);
                actionButton.setAttribute('aria-label', label);
            }
            else {
                return
            }
        });
    }
    else {
        return
    }
}
