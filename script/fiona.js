window.onload = async function () {
    const names = Array.from(await readJson('../script/names.json'));
    const nameButton = document.querySelector('#fiona-4 main button');

    nameButton.onclick = () => {
        const name = names[Math.floor(Math.random() * names.length)];
        const elem = document.createElement('p');
        elem.textContent = name;
        elem.setAttribute('tabindex', '0');
        nameButton.append(elem);
        elem.focus();
        setTimeout(() => {
            nameButton.focus();
            elem.remove();
        }, 1000);
    }
}


// UTILS

function readJson(url) {
    return fetch(url)
    .then(response => response.json())
    .catch(console.error);
}
