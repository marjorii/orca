// ACTIONS

// Find action buttons (with help of labels instead of class)
// const actionButtons = [];
// document.querySelectorAll('main *').forEach((elem) => {
//     if (elem.hasAttribute('aria-label') && elem.getAttribute('aria-label').includes('Appuyez')) {
//         actionButtons.push(elem);
//     }
// });

// Play audio on audio triggers focus
const audioTriggers = document.querySelectorAll('.audio-trigger');
audioTriggers.forEach((audioTrigger) => {

    const audio = document.createElement('audio');

    audioTrigger.onfocus = () => {
        pauseAudio()
        playAudio(audioTrigger, audio);
    }

});

// Stop audio on audio stoppers focus
const audioStoppers = document.querySelectorAll('.audio-stopper');
audioStoppers.forEach((audioStopper) => {
    audioStopper.onfocus = () => {
        pauseAudio();
    }
});

// Specific to space.html
// Play audio on button press
const buttons = document.querySelectorAll('.audio-button');
buttons.forEach((button) => {
    const audio = document.createElement('audio');
    button.onclick = () => {
        pauseAudio();
        playAudio(button, audio);
    }
    // Pause audio on ESC press
    document.addEventListener('keydown', function(e) {
        // ESC
        if (e.keyCode === 27) {
            if (audio && !audio.paused) {
                audio.pause();
                audio.currentTime = 0;
                const source = audio.src.replace(window.location.origin + '/audio/', '');
                const button = document.querySelector('button[data-source="../audio/' + source + '"]');
                button.focus();
            }
        }
    });
});

// Trigger color animations
const animTriggers = document.querySelectorAll('[class*="-anim"]');
animTriggers.forEach((animTrigger) => {
    animTrigger.onfocus = () => {
        const anim = animTrigger.className.replace('-anim', '');
        // Specific to asutralie.html
        if (anim.includes('mirage')) {
            document.querySelector('canvas').classList.add('mirage');
        }
        if (anim.includes('glow')) {
            document.body.classList.add('shade');
        }
        if (anim.includes('unlit')) {
            document.body.classList.add('erase');
        }
        if (anim.includes('noise')) {

            // Noise animation (asutralie-2.html)
            setTimeout(() => {
                document.querySelector('.background').classList.add('blink');
            }, 15000);

            setTimeout(() => {
                const noise = setInterval(glitch, 2000);
                setTimeout(() => {
                    clearInterval(noise);
                }, 5000);
            }, 17000);

            setTimeout(() => {
                document.querySelector('.background').classList.add('rewind');
            }, 30000);

        }
        document.querySelector('.background').classList.add(anim);
    }
});

// UTILS

function playAudio(audioTrigger, audio) {
    if (audio.paused) {
        audio.src = audioTrigger.dataset.source;
        audio.play();
        document.querySelector('body').appendChild(audio);
    }
    audio.onend = () => {
        audio.currentTime = 0;
    }
}

function pauseAudio() {
    document.querySelectorAll('audio').forEach((audio) => {
        if (!audio.paused) {
            audio.pause();
            audio.currentTime = 0;
        }
        else {
            return
        }
    });
}

function animate(val) {
    document.querySelector('#turbulence').setAttribute('baseFrequency', '0 ' + (val + 0.001));
    requestAnimationFrame(() => {
        animate(val + 0.001);
    });
}

function glitch() {
    setTimeout(() => {
        animate(0.001);
        document.querySelector('.background').classList.add('glitch');
    }, 1000);
    document.querySelector('.background').classList.remove('glitch');
}
